# -*- mode: ruby -*-
# vi: set ft=ruby :

#!/bin/bash
#############################################
## Vagrant file to stand up a Development  ##
## kubernetes cluster, one master, two     ##
## nodes, 2cpu each, 2G Ram each.          ##
##                                         ##
##                                         ##
## This vagrantfile is meant to be used    ##
## The Linux Academy course "kubernetes    ##
## Quick Start" by Michael McClaren        ##
## Links Below [0]                         ##
##                                         ##
## Script by john.m.rice@gmail.com         ##
##                                         ##
## Your mileage/km may vary,               ##
## Use at your own risk.                   ##
## No support is assumed or supplied by    ##
## default.                                ##
#############################################

# After writing this whole thing out by hand I found the below [1], so i stole their
# layout, put in my own scripts for CentOS and cleaned it up, and its worked great.
# their layout was much cleaner than mine so props go to them for a pretty script.
# they also helped me solve the problem of starting kubernetes in a diff IP than
# vagrants default eth device.
#
# Links and info:
# [0] https://linuxacademy.com/cp/courses/lesson/course/3606/lesson/2/module/267
# [1] https://medium.com/@wso2tech/multi-node-kubernetes-cluster-with-vagrant-virtualbox-and-kubeadm-9d3eaac28b98

#
# Setup Server baseline variables.
#

# If you edit the IPs of the instances, add more instances, or remove instances below, then you
# also need to alter the /etc/hosts file sections in the configureNoes and configureMaster scripts
# below with the correct hostnames and IPs.
servers = [
    {
        :name => "master",
        :type => "master",
        :box => "phreekbird/kube-centos7-base",
        :box_version => "2",
        :eth1 => "192.168.232.10",
        :mem => "2048",
        :cpu => "2"
    },
    {
        :name => "node1",
        :type => "node",
        :box => "phreekbird/kube-centos7-base",
        :box_version => "2",
        :eth1 => "192.168.232.11",
        :mem => "2048",
        :cpu => "2"
    },
    {
        :name => "node2",
        :type => "node",
        :box => "phreekbird/kube-centos7-base",
        :box_version => "2",
        :eth1 => "192.168.232.12",
        :mem => "2048",
        :cpu => "2"
    }
]

# "There is no cow level"

#
# Setup bootstrap for Nodes
#
$configureNodes = <<-SCRIPT
#If you need/want to debug, uncomment the set xtrace.
#set -o xtrace
#force swap off and delete swapfile
swapoff /swapfile
rm -rf swapfile
#sync repos
yum makecache fast
yum repolist
#upgrade the system
yum upgrade -y
#fix etc/hosts
mv /etc/hosts /etc/hosts.ORIG.BAK
cat << EOF > /etc/hosts
127.0.0.1 localhost localhost.localdomain
192.168.232.10 master.kube master
192.168.232.11 node1.kube node1
192.168.232.12 node2.kube node2
EOF
systemctl enable docker
systemctl enable kubelet
systemctl start docker
systemctl start kubelet
systemctl daemon-reload
systemctl restart kubelet
echo "1" > /proc/sys/net/bridge/bridge-nf-call-iptables
#Join the cluster
curl master/kubeadm_join_cmd.sh > /tmp/kubeadm_join_cmd.sh
chmod a+x /tmp/kubeadm_join_cmd.sh
/tmp/kubeadm_join_cmd.sh
SCRIPT

#
# Setup bootstrap for Master
#
$configureMaster = <<-SCRIPT
#If you need/want to debug, uncomment the set xtrace.
#set -o xtrace
#force swap off and delete swapfile
swapoff /swapfile
rm -rf swapfile
#sync repos
yum makecache fast
yum repolist
#upgrade the system
yum upgrade -y
#fix etc/hosts
mv /etc/hosts /etc/hosts.ORIG.BAK
cat << EOF > /etc/hosts
127.0.0.1 localhost localhost.localdomain
192.168.232.10 master.kube master
192.168.232.11 node1.kube node1
192.168.232.12 node2.kube node2
EOF
#verify docker and kubelet are started at boot.
systemctl enable docker
systemctl enable kubelet
systemctl start docker
systemctl start kubelet
SCRIPT

# It's in that place i put that thing that time... -Hackers

#
# Now lets do the Vagrant stuffs....
#
Vagrant.configure("2") do |config|

    servers.each do |opts|
        config.vm.define opts[:name] do |config|

            config.vm.box = opts[:box]
            config.vm.hostname = opts[:name]
            config.vm.network :private_network, ip: opts[:eth1]

            config.vm.provider "virtualbox" do |v|

              v.name = opts[:name]
              v.customize ["modifyvm", :id, "--groups", "/Kubernetes Development"]
              v.customize ["modifyvm", :id, "--memory", opts[:mem]]
              v.customize ["modifyvm", :id, "--cpus", opts[:cpu]]
            end

            if opts[:type] == "master"
                config.vm.provision "shell", inline: $configureMaster
                config.vm.provision "file", source: "./masterBootstrap.sh", destination: "/tmp/masterBootstrap.sh"

            else opts[:type] == "nodes"
                config.vm.provision "shell", inline: $configureNodes
            end

        end
    end
end

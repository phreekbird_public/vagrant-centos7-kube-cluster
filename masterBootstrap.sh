#!/bin/bash
# install k8s master
# if not already done, force swapfile off
swapoff /swapfile
rm -rf /swapfile
# force the bridge nf chagnes, they always fail so we do it right before a kube init.
echo "1" > /proc/sys/net/bridge/bridge-nf-call-iptables
#initialize kubernetes.
kubeadm init --apiserver-advertise-address=IP_ADDR --apiserver-cert-extra-sans=HOST_FQDN --node-name=HOSTNAME --pod-network-cidr=192.168.0.0/16
#copy kube config for root user.
mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config
#copy kube config for vagrant user.
mkdir -p /home/vagrant/.kube
cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
chown vagrant:vagrant /home/vagrant/.kube/config
#initialize flanel networking plugin
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
#Setup our kube join script
KUBETOKEN=`kubeadm token list | awk '{print $1}' | tail -n +2`
KUBEDISCOVERYTOKEN=`openssl x509 -in /etc/kubernetes/pki/ca.crt -noout -pubkey | openssl rsa -pubin -outform DER 2>/dev/null | sha256sum | cut -d' ' -f1`
echo "kubeadm join $IP_ADDR:6443 --token $KUBETOKEN --discovery-token-ca-cert-hash sha256:$KUBEDISCOVERYTOKEN" > /tmp/kubeadm_join_cmd.sh
chmod a+x /tmp/kubeadm_join_cmd.sh
# setup epel repo
yum install epel-release -y
yum repolist
#setup tinyhttp server so our instances can get access to the script
yum install lighttpd -y
#start up http server
systemctl start lighttpd
# move the kube join script over so its available via http
cp /tmp/kubeadm_join_cmd.sh /var/www/lighttpd/kubeadm_join_cmd.sh
# because were scrubs and dont isntall a cgi handler and other things for lighttpd
# we need to edit some lighttpd conf files.
sed -i -e 's/server.use-ipv6 = "enable"/server.use-ipv6 = "disable"/g' /etc/lighttpd/lighttpd.conf #who needs IPv6?
sed -i -e 's/server.max-connections = 1024/server.max-connections = 4096/g' /etc/lighttpd/lighttpd.conf
systemctl restart lighttpd
echo ""
echo ""
echo ""
echo "lighttpd service started"
echo "YOU SHOULD STOP lighttpd SERVICE WHEN YOU ARE DONE JOINING KUBE NODES!"
echo "systemctl stop lighttpd"
echo ""
echo ""
echo ""

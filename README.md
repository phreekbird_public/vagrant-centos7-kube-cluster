# Vagrantfile for CentOS7 Kubernetes Cluster.

This entire thing was made to use with the Linux Academy Kubernetes Quick Start Course [0].

After writing this whole thing out by hand I found the below [1], so i stole their layout, put in my own scripts for CentOS and cleaned it up, and its worked great. Their layout was much cleaner than mine so props go to them for a pretty script.

Vagrant box used in this is CentOS with kube installed and virtualbox tools. [2]

Links and info:

[0] https://linuxacademy.com/cp/courses/lesson/course/3606/lesson/2/module/267

[1] https://medium.com/@wso2tech/multi-node-kubernetes-cluster-with-vagrant-virtualbox-and-kubeadm-9d3eaac28b98

[2] https://app.vagrantup.com/phreekbird/boxes/kube-centos7-base

[3] https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/

This project is in its early stages. _There may be bugs!_

This project *assumes* a few things:

  - **Docker**: This project assumes you know basic about Docker.
  - **Linux**: This project assumes you know basic about bash/Linux.
  - **Vagrant**: You have vagrant installed, with virtualbox, and know the basics.
  - **Kubernetes**: This entire thing is inteded to setup a kube lab, to learn kube, but it helps if you have watched a video or read something about kubernetes first...

This project is provided "as-is". Your mileage/km may very. I am not /will not be/ held responsible if you screw something up. You are provided with no type of warranties, or guarantees at all.

Good luck, and may the force be with you...

## System requirements
roughly 10G of HD Space
6 Gigs of RAM
* each vagrant instance creates 2 cpu's, 2 Gigs of RAM, and a thin provisioned 40GB HD.

## But what does it do?

- The Vagrantfile is used to generate a master node, pre-installed with all the packages needed to stand up a kubernetes cluster.
- Once the master node is up simply run vagrant up for the nodes... they should automagically discover and join the cluster.

## What it doesn't do...

- This thing will not automagically start the master node... Why? well I am glad you asked. (let me take my geek out...) Simply put, kubeadm refuses to initialize a cluster using variable/aliases ... so, I cannot cat/grep/bash magic the IP, hostname, and FQDN, then have kubeadm use those variables and initialize. So yes, you will need to edit one file. but dont worry i provide a guide... see below.

## Quickstart

### TL:DR
1. stand up the master node
```
vagrant up master
```
2. Edit the kube init file.
```
vim /tmp/masterBootstrap.sh 
```
3. Stand up Kube Master
```
/tmp/masterBootstrap.sh
```
- copy the join command that is printed to the screen.
4. Edit the Kube Join Script
```
echo "PASTE YOUR PERSONAL JOIN SCRIPT HERE" > /var/www/lighttpd/kubeadm_join_cmd.sh
```
5. Stand up the nodes.
```
vagrant up node1 node2
```
6. Deploy dashboard.
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
kubectl proxy --address='192.168.232.10' --accept-hosts='^localhost$,^0\.0\.0\.0$,^\[::0\]$' &
```

### Git global setup

 - If you havn't already, setup your git config.

```
git config --global user.name "Your Username"
git config --global user.email "youremail@here"
```

### Clone the repo
```
git clone git@gitlab.com:phreekbird_public/vagrant-centos7-kube-cluster.git
cd vagrant-centos7-kube-cluster
```

### Whos the Masta!?!
Stand up the master node.
```
vagrant up master
```
### Sho Nuff ... get the IP, FQDN, and Hostname
Log into the Master node.
```
vagrant ssh master
```
Now runt he following commands to get a print out of the IP, Hostname, and FQDN. Copy and save the output ... your going to need it to edit the files later.
```
IP_ADDR=`ip add | grep eth1 | awk '{print $2}' | cut -f2 -d: | cut -f1 -d/`
HOST_FQDN=`cat /etc/hosts | grep master.kube | awk '{print $2}'`
HOSTNAME=$(hostname)
echo "IP ADDR = "$IP_ADDR && echo "Hostname FQDN = "$HOST_FQDN && echo "Hostname = "$HOSTNAME
```

### Edit masterBootstrap.sh
In the masterBootstrap.sh file, the line 'kubeadm init --apiserver-advertise-address=IP_ADDR --apiserver-cert-extra-sans=HOST_FQDN --node-name=HOSTNAME --pod-network-cidr=192.168.0.0/16' needs to be changed. Unfortunately, kubeadm dosn't let us assign the above variables, and just call them, for example, kubeadm init --apiserver-advertise-address=$IP_ADDR will not return the IP address... it returns nothing. So... get to editing that file by hand because I'm too stupid/tired/lazy to figure out how to automate this... who knows, maybe ill fix this  later and fully automate the whole thing.

You can edit via vim
```
vim /tmp/masterBootstrap.sh
```
or try this... good luck. =D
```
sed -i -e 's/--apiserver-advertise-address=IP_ADDR/--apiserver-advertise-address=$IP_ADDR/g' /tmp/masterBootstrap.sh
sed -i -e 's/--apiserver-cert-extra-sans=HOST_FQDN/--apiserver-cert-extra-sans=$HOST_FQDN/g' /tmp/masterBootstrap.sh
sed -i -e 's/--node-name=HOSTNAME/--node-name=$HOSTNAME/g' /tmp/masterBootstrap.sh
```
Now is also a good time to apy attention to the  --pod-network-cidr=192.168.0.0/16 section.
You can edit this to suite your own internal networking needs. In my case my home network has 192.168.1.0 subnet, so lets specifcy something different for our pods.
ex: 10.212.0.0/16 will give us a signle class B subnet fo rour pods to deploy to.

I would suggest cat-ing out the /tmp/masterBootstrap.sh file first, to make sure the edits took.
```
cat /tmp/masterBootstrap.sh
```
### Stand up the cluster
Login as root
```
sudo su -
```
Run the bootsrtap script.
```
/tmp/masterBootstrap.sh
```
This will initialize the kube cluster and print out a ton of info for you...
Once done, you can copy/paste the jopin script kubeadm gives you, however you should have one already in /var/www/lighttpd/kubeadm_join_cmd.sh
This script should automatically be downloaded by the nodes and kubeadm should join the cluster.

Make sure you verify the contents of /var/www/lighttpd/kubeadm_join_cmd.sh with the output that was printed to the screen during the kube init.
If they are different, edit that join script and make it exactly like the one on the screen.
Sometimes I have noticed when the script refuses to snag the tokens and IP of the API server, so this is why i ask you to verify it.

An easy way to edit your personalized join script is to copy the join script that is outputed from kube init... then do the following.
```
echo "PASTE YOUR PERSONAL JOIN SCRIPT HERE" > /var/www/lighttpd/kubeadm_join_cmd.sh
```

Once you have verified
1. The master node is up and running.
2. The /var/www/lighttpd/kubeadm_join_cmd.sh join script exists... and is correct.
3. That the api server is listening on your IP, and that IP should also exists in the join script.

Then your ready to move on to the nodes.

### Release the nodes!
```
vagrant up node1 node2
```
You can add more nodes to the vagrantfile, just make sure to bring them up too.

### Verify the cluster nodes
As root on your master node run:
```
kubectl get nodes
```
That should list out all the nodes in your kube cluster

### Get a nice UI Dashboard
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
kubectl proxy --address='192.168.232.10' --accept-hosts='^localhost$,^0\.0\.0\.0$,^\[::0\]$' &
```
Based on [3] 


## License

GNU General Public License v3.0

## Author Information
- @john.m.rice

## Dependencies:
- Docker
- Linux know-how
- bash
- kubernetes

_Use at your own risk, your mileage may vary..._
